#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
  clock_t begin, end;
  double time_spent;

  begin = clock();
  srand((unsigned int) time(NULL));
  int depozit, broj_rundi, izvuceni_broj, tip, ulog, boja, broj, kuca=0; //depozit=iznos s kojim svi igrači kreću
  int *broj_igraca= new int;


  cout<<"Broj rundi: ";
  cin>>broj_rundi;
  
  cout<<"Broj igraca: ";
  cin>>*broj_igraca;

  cout<<"Depozit: ";
  cin>>depozit;

  int *igraci = new int[*broj_igraca]; //polje u koje se sprema koliko svaki igrač ima para

  for (int i=0; i<*broj_igraca; i++)
    {
      igraci[i]=depozit; //postavljanje balansa svim igračima na vrijednost depozita
    }

  for (int i=0; i<broj_rundi; i++)
    {
      izvuceni_broj=rand()%36;
      for (int j=0; j<*broj_igraca; j++)
	{
	  tip=rand()%2;
	  ulog=(rand()%(depozit-1)+1);
	  cout<<"Igrac "<<j+1<<" ulaze "<<ulog<<endl;
	  if (tip==0) //tip 0 je klađenje na boju i dobitak je 2 puta uloženo
	    {
	      boja=rand()%2;
	      if ((izvuceni_broj!=0) && ((boja==0 && izvuceni_broj%2==0) || (boja==1 && izvuceni_broj%2!=0))) //kod klađenja na boje u slučaju da je izvučena 0, igrač automatski gubi, parni brojevi su "crveni", a neparni "crni", boja==0->crvena, boja==1->crna
		{
		  igraci[j]+=ulog;
		  kuca-=ulog;
		  cout<<"Igrac "<<j+1<<" je dobio "<<ulog<<" kn na bojama";
		}
	      else
		{
		  igraci[j]-=ulog;
		  kuca+=ulog;
		  cout<<"Igrac "<<j+1<<" je izgubio "<<ulog<<" kn na bojama";
		}
	      cout<<" te sad ima "<<igraci[j]<<endl;
	    }
	  else // tip 1 je klađenje na broj i dobitak je 35 puta uloženo
	    {
	      broj=rand()%36;
	      if (izvuceni_broj==broj)
		{
		  igraci[j]+=ulog*34;
		  cout<<"Igrac "<<j+1<<" je pogodio broj "<<izvuceni_broj<<" i dobio "<<ulog*35<<" kn";
		  kuca-=ulog*34;
		}
	      else
		{
		  igraci[j]-=ulog;
		  cout<<"Igrac "<<j+1<<" je izgubio "<<ulog<<" kn na pogadjanju";
		  kuca+=ulog;
		}
	      cout<<" te sad ima "<<igraci[j]<<endl;
	    }
	  
	}
    }
  cout<<"Rezultati: "<<endl;
  for (int i=0;i<*broj_igraca; i++)
    {
      cout<<i+1<<". igrac ima "<<igraci[i]<<endl;
    }
  cout<<"Kuca (je uvijek u plusu) balans: "<<kuca<<endl;
  end = clock();
  time_spent = (double)(end - begin);
  cout<<"Vrijeme CPU: "<<time_spent/1000<<" sekundi"<<endl;
  system("pause");
  return 0;
}
