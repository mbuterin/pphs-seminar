import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import pycuda.curandom as cr
import pycuda.cumath as cm
from pycuda.compiler import SourceModule
import numpy as np

mod = SourceModule(open("seminar.cu").read())

simulacija = mod.get_function("simulacija")

broj_rundi = 1024
br_igraca = 100
max_ulog = 1000

igraci = cm.floor((max_ulog+1) * cr.rand((broj_rundi, br_igraca), dtype=np.float32))
odabrani_brojevi = cm.floor(37 * cr.rand((broj_rundi, br_igraca), dtype=np.float32))
izvuceni_brojevi = cm.floor(cr.rand(broj_rundi, dtype=np.float32) * 37)
rezultat_rundi = ga.empty((broj_rundi, br_igraca), dtype=np.float32)

simulacija(igraci, odabrani_brojevi, izvuceni_brojevi, rezultat_rundi, block=(broj_rundi,1,1), grid=(br_igraca,1))

rezultat_rundi = rezultat_rundi.get().sum(axis=0)

i=1
print "Nakon ", broj_rundi, "rundi, stanje svakog igraca je:"
for igrac in rezultat_rundi:
    print i, ". igrac je zavrsio sa", igrac
    i=i+1
