#include <stdio.h>

__global__ void simulacija(float * igraci, float * odabrani_brojevi, float * izvuceni_brojevi, float * rez){  

  const int i = blockIdx.x * blockDim.x + threadIdx.x;
  const int j = blockIdx.x;

  if (odabrani_brojevi[i] == izvuceni_brojevi[j]){
    rez[i] = igraci[i] * 35;
  }else{
    rez[i] = -1 * igraci[i];
  }

}
